package com.mtk.iflash;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static com.mtk.iflash.R.id;
import static com.mtk.iflash.R.layout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CameraManager mCameraManager;
    private ImageView ivSOS, ivPower;
    private String mCamerald;
    private boolean mFlash=true;
    private Thread th;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);
        initView();
    }

    private void initView() {
        findViewById(id.iv_power).setOnClickListener(this);
        findViewById(id.iv_sos).setOnClickListener(this);

        ivPower = findViewById(id.iv_power);
        ivSOS = findViewById(id.iv_sos);

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCamerald = mCameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case id.iv_sos:
                doSOS();
                break;
            case id.iv_power:
                doFlash();
                break;

        }
    }

    private void doFlash() {
        if(mFlash){
            try {
                mCameraManager.setTorchMode(mCamerald,false);
                ivPower.setBackground(getDrawable(R.drawable.ic_poweroff));
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            mFlash=false;
        }else{
            try {
                mCameraManager.setTorchMode(mCamerald, true);
                ivPower.setBackground(getDrawable(R.drawable.ic_poweron));
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            mFlash=true;
        }
    }

    public void doSOS() {
        Runnable rb = new Runnable() {
            @Override
            public void run() {
                execTask();
            }
        };
        th = new Thread(rb);
        th.setDaemon(true);
        th.start();
    }

    private void execTask() {
        int time = 0;
        while (true) {
            if (time > 1000) {
                time = 0;
            }
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } 
        }
    }
}
